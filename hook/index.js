async function preInit(inputObj) {

}

async function postInit(inputObj) {
    console.log(`\n     _______     _             
    |___  / |   | |            
       / /| |__ | | ___   __ _ 
      / / | '_ \\| |/ _ \\ / _\` |
    ./ /__| |_) | | (_) | (_| |
    \\_____/_.__/|_|\\___/ \\__, |
                          __/ |
                         |___/ 
                                        `)
    console.log(`\n    Welcome to the start-zblog application
     This application requires to open these services: 
         FC : https://fc.console.aliyun.com/
         ACR: https://cr.console.aliyun.com/
     This application can help you quickly deploy the Zblog project:
         Full yaml configuration: https://github.com/devsapp/zblog#%E5%AE%8C%E6%95%B4yaml
         Zblog development docs : https://docs.zblogcn.com/
     This application homepage: https://github.com/devsapp/start-zblog\n`)
}

module.exports = {
    postInit,
    preInit
}
